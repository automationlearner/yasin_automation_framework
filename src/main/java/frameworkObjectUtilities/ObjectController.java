package frameworkObjectUtilities;

public class ObjectController {

	public static Object getInstance(String className) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		Class<?> obj = Class.forName(className);
	      
        ClassLoader loader = obj.getClassLoader();
        
          return obj;
		
	}
	
}
