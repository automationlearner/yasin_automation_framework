package javaScriptExecutor;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import utilities.ConfigurationClass;

public class Scrolling extends ConfigurationClass {

	WebDriver driver = invokeSeleniumSetUp();

	@Test(priority = 4)
	public void performPageDownScrolling() {
		driver.get("http://moneyboats.com/");
		driver.manage().window().maximize();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,600)");
	}

	@Test(priority = 3)
	public void scrollTillVerticallyElementVisible() {

		driver.get("http://demo.guru99.com/test/guru99home/");
		driver.manage().window().maximize();
		WebElement element = driver.findElement(By.linkText("Linux"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", element);

	}

	@Test(priority = 1)
	public void scrollDownToBottomPage() {
		driver.get("http://demo.guru99.com/test/guru99home/");
		driver.manage().window().maximize();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
	}
	
	
	@Test(priority = 2)
	public void scrollHorizontallyTillElementVisible() {

		driver.get("http://demo.guru99.com/test/guru99home/scrolling.html");
		driver.manage().window().maximize();
		WebElement element = driver.findElement(By.xpath("//a[text()='VBScript']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", element);

	}

}
