package javaConcepts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.testng.annotations.Test;

public class Programmings {
	int smallest;
	int highest;
	int firstMax;
	int secondMax;
	int listIndex = 0;
	int elementIndex = 0;

	@Test
	public void frequencyOfOccurenceInIntegerArray() {
		int[] a = { 1, 2, 4, 5, 1, 2, 1, 2, 4, 5, 7, 1, 2 };
		List<Integer> intList = Arrays.stream(a).boxed().collect(Collectors.toList());
		Set<Integer> set = new HashSet<Integer>(intList);

		System.out.println("*************using set and list******************");
		for (int k : set) {
			int frequency = Collections.frequency(intList, k);
			System.out.println(k + " occurred in list for " + frequency + " times");
		}

		System.out.println("**************using map **********************");
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int s : a) {
			if (map.containsKey(s)) {
				map.put(s, map.get(s) + 1);
			} else {
				map.put(s, 1);
			}
		}

		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			System.out.println(entry.getKey() + "   -->   " + entry.getValue());
		}

	}

	@Test
	public void frequencyOfOccurenceInStringArray() {
		String[] str = { "yasin", "asif", "shahin", "yasin", "kalim", "yasin" };
		List<String> lists = Arrays.asList(str);
		Set<String> set = new HashSet<String>(lists);
		int frequency = Collections.frequency(lists, "yasin");
		System.out.println("Yasin occurred in list for " + frequency + " times");
		for (String s : set) {
			int frequencies = Collections.frequency(lists, s);
			System.out.println(s + " occurred in list for " + frequencies + " times");
		}

	}

	@Test
	public void CharacterFrequencyOfOccurenceInString() {
		String str = "p,a,r,t,i,c,i,p,a,n,t,s";
		List<String> lists = new ArrayList<String>(Arrays.asList(str.split(",")));
		Set<String> set = new HashSet<String>(lists);

		for (String s : set) {
			int frequencies = Collections.frequency(lists, s);
			System.out.println(s + " occurred in list for " + frequencies + " times");
		}

	}

	@Test
	public void removeDublicates() {
		String[] str = { "yasin", "asif", "shahin", "yasin", "kalim", "yasin" };
		List<String> lists = Arrays.asList(str);
		Set<String> set = new HashSet<String>(lists);

		for (String x : set) {
			System.out.println(x);
		}

		String s = "automation";

		LinkedHashSet<Character> setChar = new LinkedHashSet<Character>();
		for (int i = 0; i < s.length(); i++) {
			setChar.add(s.charAt(i));
		}
		String duplicate = "";
		for (Character ch : setChar) {
			duplicate = duplicate + ch;

		}
		System.out.print(duplicate);
	}

	@Test
	public void reverse2DArray() {
		int[][] a = { { 1, 2, 7 }, { 4, 7, 9 }, { 7, 5, 6 } };
		System.out.println("printing 2darray normally");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(a[i][j] + " ");
			}

			System.out.println();
		}

		System.out.println("print in reverse order");
		for (int k = 2; k >= 0; k--) {
			for (int l = 2; l >= 0; l--) {
				System.out.println(a[k][l]);
			}
		}
	}

	@Test
	public void smallestIn2DArray() {
		int[][] a = { { 9, 2, 7 }, { 4, 7, 9 }, { 7, 5, 6 } };
		smallest = a[0][0];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {

				if (smallest > a[i][j]) {
					smallest = a[i][j];
				}

			}
		}

		System.out.println("smallest amongs 2d array = " + smallest);
	}

	@Test
	public void HighestInRowWhereSmallestArrayFound() {
		int[][] a = { { 7, 8, 2 }, { 5, 6, 4 }, { 1, 3, 9 } };
		smallest = a[0][0];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {

				if (smallest > a[i][j]) {

					smallest = a[i][j];
					highest = a[i][0];
					for (int k = 0; k < 3; k++) {
						if (highest < a[i][k]) {
							highest = a[i][k];
						}
					}
				}
			}

		}

		System.out.println("smallest value in 2d Array = " + smallest);
		System.out.println("Highest value present in the row where smallest value in overall array found = " + highest);

	}

	@Test
	public void HighestInColumnWhereSmallestArrayFound() {
		int[][] a = { { 7, 8, 2 }, { 5, 6, 4 }, { 1, 3, 9 } };
		smallest = a[0][0];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {

				if (smallest > a[i][j]) {

					smallest = a[i][j];
					highest = a[0][j];
					for (int k = 0; k < 3; k++) {
						if (highest < a[k][j]) {
							highest = a[k][j];
						}
					}
				}
			}

		}

		System.out.println("smallest value in 2d Array = " + smallest);
		System.out.println(
				"Highest value present in the column where smallest value in overall array found = " + highest);

	}

	@Test
	public void stringConcepts() {
		StringBuilder s = new StringBuilder("yasin raza is a automation tester");
		s.reverse();

		System.out.println(s);

		String str = "lets do it man";
		String[] split = str.split("\\s");
		for (String x : split) {
			System.out.println(x);
		}

	}

	@Test
	public void sortTheListOfStrings() {

		String[] inputList = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
		System.out.println("**************displaying unsorted list********************");
		for (String ar : inputList) {

			System.out.println(ar);
		}

		List<String> sorts = new LinkedList<String>(Arrays.asList(inputList));
		System.out.println("**********sorted list**************");
		Collections.sort(sorts);
		for (String sort : sorts) {

			System.out.println(sort);
		}
	}

	@Test
	public void reverseNumber() {
		int input = 143;
		int reverse = 0;
		while (input != 0) {
			reverse = (reverse * 10) + (input % 10);
			input = input / 10;

		}
		System.out.println("reverse of a number = " + reverse);
	}

	@Test
	public void checkPrimeNumber() {

		int input = 17;
		boolean isPrime = true;
		for (int i = 2; i < Math.sqrt(input); i++) {
			if (input % i == 0) {
				System.out.println("Not a prime number");
				isPrime = false;
				break;
			}

		}
		System.out.println("Is input value a prime number? " + isPrime);
	}

	@Test
	public void firstTwoMaximumIn2DArray() {
		int[][] array = { { 2, 4, 199 }, { 58, 226, 41 }, { 98, 19, 1 } };
		List<List<Integer>> arrays = Arrays.stream(array)
				.map(a -> Arrays.stream(a).boxed().collect(Collectors.toList())).collect(Collectors.toList());

		int listSize = arrays.size();

		firstMax = arrays.get(0).get(0);

		for (int i = 0; i < listSize; i++) {
			for (int j = 0; j < 3; j++) {

				if (arrays.get(i).get(j) > firstMax) {
					firstMax = arrays.get(i).get(j);
					listIndex = i;
					elementIndex = j;

				}
			}

		}

		System.out.println("maximum value in an 2D array = " + firstMax);

		arrays.get(listIndex).remove(elementIndex);

		secondMax = arrays.get(0).get(0);

		for (List<Integer> abc : arrays) {

			for (int element : abc) {
				if (element > secondMax) {
					secondMax = element;
				}
			}

		}
		System.out.println("second maximum value in an 2D array = " + secondMax);
	}

	@Test
	public void firstTwoMaximumIn1DArray() {
		int[] array = { 15, 214, 48, 21, 43, 11, 79, 93 };
		List<Integer> arrays = Arrays.stream(array).boxed().collect(Collectors.toList());
		int listSize = arrays.size();
		firstMax = arrays.get(0);
		for (int i = 0; i < listSize; i++) {
			if (arrays.get(i) > firstMax) {
				firstMax = arrays.get(i);
				elementIndex = i;
			}
		}
			System.out.println("maximum value in an 1D array = " + firstMax);

			arrays.remove(elementIndex);
			secondMax = arrays.get(0);

			for (int elements : arrays) {
				if (elements > secondMax) {
					secondMax = elements;
				}
			}

			System.out.println("second maximum value in an 1D array = " + secondMax);

		}

	}
