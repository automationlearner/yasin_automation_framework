package toolTipConcepts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import utilities.ConfigurationClass;

public class ToolTip extends ConfigurationClass {

	WebDriver driver = invokeSeleniumSetUp();

	@Test
	public void toolTipConcepts() {
		driver.get("http://demo.guru99.com/test/tooltip.html");
		driver.manage().window().maximize();
		Actions action = new Actions(driver);
		action.clickAndHold().moveToElement(driver.findElement(By.xpath("//a[text()='Download now']"))).build()
				.perform();
		
		String fileName= driver.findElement(By.xpath("//*[@id=\"demo_content\"]/div/div/div/table/tbody/tr[1]/td[2]")).getText();
		System.out.println(fileName);
	}

}
