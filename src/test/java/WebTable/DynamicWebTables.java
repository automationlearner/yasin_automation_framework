package WebTable;

import java.util.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actionTemplates.ActionDefination;


public class DynamicWebTables {
	WebDriver driver;
	WebElement element;
	ActionDefination commons = new ActionDefination();
	
	@BeforeClass

	public void seleniumSetupConfiguration() {
		System.setProperty("webdriver.chrome.driver", "Y:\\chrome 90\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@BeforeMethod
	public void openWebTables() {
		String urls = "http://demo.guru99.com/test/web-table-element.php";
		commons.navigateToWebsite(driver, urls);
	}

	@Test
	public void currentPriceOfSpecificCompany() {

		// calculate no of columns
		List<WebElement> column = driver.findElements(By.xpath("//*[@id='leftcontainer']/table/thead/tr/th"));
		System.out.println("coulumn size = " + column.size());

		// calculate no of rows
		List<WebElement> rows = driver.findElements(By.xpath("//*[@id='leftcontainer']/table/tbody/tr"));
		System.out.println("row size = " + rows.size());

		for (int i = 0; i < rows.size(); i++) {

			String currentPriceXpath = "//*[@id='leftcontainer']/table/tbody/tr[" + (i + 1) + "]/td[4]";

			String companyName = "ICICI Pru Life";
			
			String actualCompanyNameXpath = "//*[@id='leftcontainer']/table/tbody/tr["+(i+1)+"]/td[1]/a";
					//rows.get(i).getText();
			String actualCompanyName = driver.findElement(By.xpath(actualCompanyNameXpath)).getText();
			// System.out.println(actualCompanyName);
			if (actualCompanyName.contains(companyName)) {
				String currentPrice = driver.findElement(By.xpath(currentPriceXpath)).getText();
				System.out.println("Current price of " + companyName + " is : " + currentPrice);
			}

		}

	}

	@Test
	public void currentPriceOfEveryCompany() {

		// calculate no of columns
				List<WebElement> column = driver.findElements(By.xpath("//*[@id='leftcontainer']/table/thead/tr/th"));
				System.out.println("coulumn size = " + column.size());

				// calculate no of rows
				List<WebElement> rows = driver.findElements(By.xpath("//*[@id='leftcontainer']/table/tbody/tr"));
				System.out.println("row size = " + rows.size());

				
		System.out.println("companyName                    currentPrice" );
		for (int i = 0; i < rows.size(); i++) {

			String currentPriceXpath = "//*[@id='leftcontainer']/table/tbody/tr[" + (i + 1) + "]/td[4]";
			String companyNameXpath = "//*[@id='leftcontainer']/table/tbody/tr[" + (i + 1) + "]/td[1]";
			String currentPrice = driver.findElement(By.xpath(currentPriceXpath)).getText();
			String companyName = driver.findElement(By.xpath(companyNameXpath)).getText();
			
			System.out.println(companyName+"                                 "+currentPrice);

		}
	}

}
