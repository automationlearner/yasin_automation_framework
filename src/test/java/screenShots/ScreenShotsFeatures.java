package screenShots;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import utilities.ConfigurationClass;

public class ScreenShotsFeatures extends ConfigurationClass {

	WebDriver driver = invokeSeleniumSetUp();

	public void takeFullScreenshots() throws IOException {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destinationFilePath = System.getProperty("user.dir") + "\\reports\\fullscreenshots.png";
		File destinationFile = new File(destinationFilePath);
		FileUtils.copyFile(source, destinationFile);

	}

	public void takeElementScreenshots(WebElement element) throws IOException {

		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		BufferedImage fullImage = ImageIO.read(source);
		Point point = element.getLocation();
		int width = element.getSize().getWidth();
		int height = element.getSize().getHeight();
		BufferedImage elementImage = fullImage.getSubimage(point.getX(), point.getY(), width, height);
		ImageIO.write(elementImage, "png", source);
		String destinationFilePath = System.getProperty("user.dir") + "\\reports\\elementscreenshots.png";
		File destinationFile = new File(destinationFilePath);
		FileUtils.copyFile(source, destinationFile);

	}

	
	@Test
	public void screenshotTesting() throws IOException {
		
		driver.get("http://www.google.com");
		WebElement ele = driver.findElement(By.id("hplogo"));
		
		takeFullScreenshots();
		takeElementScreenshots(ele);
	}
}
