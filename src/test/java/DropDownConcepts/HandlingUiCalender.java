package DropDownConcepts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class HandlingUiCalender {
	public static void selectDateFromCalender(WebDriver driver, String expectedMonth, String expectedDate)
			throws InterruptedException {
		
		while (true) {
			System.out.println("enetered in while loop");
			Thread.sleep(3000);

			List<WebElement> months = driver.findElements(By.xpath("//div[@class='ui-datepicker-title']"));

			String actualMonthFirst = months.get(0).getText();
			System.out.println(actualMonthFirst);
			
			String actualMonthlast = months.get(1).getText();
			System.out.println(actualMonthlast);
			if (actualMonthFirst.equalsIgnoreCase(expectedMonth)) {

				List<WebElement> dates = driver.findElements(By.xpath(
						"//div[@class='ui-datepicker-group ui-datepicker-group-first']//td[@data-handler='selectDay']//a"));

				for (WebElement date : dates) {
					String actualDate = date.getText();
					if (actualDate.equalsIgnoreCase(expectedDate))
						Thread.sleep(2000);
					date.click();

					break;
				}
			} else if (actualMonthlast.equalsIgnoreCase(expectedMonth)) {

				List<WebElement> dates = driver.findElements(By.xpath(
						"//div[@class='ui-datepicker-group ui-datepicker-group-last']//td[@data-handler='selectDay']//a"));

				for (WebElement date : dates) {
					String actualDate = date.getText();
					if (actualDate.equalsIgnoreCase(expectedDate))
						Thread.sleep(2000);
					date.click();
					break;

				}
			} else {
				Thread.sleep(7000);

				driver.findElement(By.xpath("//a[@class='ui-datepicker-next ui-corner-all']")).click();
			}

			

		}
	}

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "Y:\\chrome 90\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get("http:\\www.spicejet.com");
		driver.manage().window().maximize();
		Thread.sleep(2000);

		// ***********************one way scenario*************************
		// check one way radio button is default selected

		boolean oneWay = driver.findElement(By.xpath("//input[@id='ctl00_mainContent_rbtnl_Trip_0']")).isEnabled();
		System.out.println("is one way enable? : " + oneWay);
		Assert.assertTrue(oneWay);

		// check return date is default disabled

		String disableReturnDate = driver.findElement(By.xpath("//div[@id='Div1']")).getAttribute("style");
		System.out.println(disableReturnDate);
		if (disableReturnDate.contains("0.5")) {
			System.out.println("return date is disabled");
			Assert.assertTrue(true);
		} else {
			Assert.assertTrue(false);
		}

		// ******************select departure and arrival city**************************
		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_ddl_originStation1_CTXT']")).click();
		driver.findElement(By.xpath("//a[text()=' Ranchi (IXR)']")).click();
		Thread.sleep(2000);
		// using index concepts
		driver.findElement(By.xpath("(//a[text()=' Kolkata (CCU)'])[2]")).click();
		Thread.sleep(2000);

		// using parent child concepts

		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_ddl_destinationStation1_CTXT']")).click();
		driver.findElement(By
				.xpath("//div[@id='glsctl00_mainContent_ddl_destinationStation1_CTNR'] //a[text()=' Srinagar (SXR)']"))
				.click();

		Thread.sleep(2000);

		selectDateFromCalender(driver, "August 2021", "25");
	}

}
