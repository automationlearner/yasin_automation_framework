package DropDownConcepts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class HandlingDropDowns {

	public void selectDateFromCalender(WebDriver driver, String expectedMonth, String expectedDate) {
		int i = 0;
		while (i < 7) {

			List<WebElement> months = driver.findElements(By.xpath("//div[@class='ui-datepicker-title']"));

			String actualMonthFirst = months.get(0).getText();
			String actualMonthlast = months.get(1).getText();
			if (actualMonthFirst.equalsIgnoreCase(expectedMonth)) {

				List<WebElement> dates = driver.findElements(By.xpath(
						"//div[@class='ui-datepicker-group ui-datepicker-group-first']//td[@data-handler='selectDay']//a"));

				for (int j = 0; j < dates.size(); j++) {
					String actualDate = dates.get(i).getText();
					if (actualDate.equalsIgnoreCase(expectedDate))
						dates.get(i).click();

					break;
				}
			} else if (actualMonthlast.equalsIgnoreCase(expectedMonth)) {

				List<WebElement> dates = driver.findElements(By.xpath(
						"//div[@class='ui-datepicker-group ui-datepicker-group-last']//td[@data-handler='selectDay']//a"));

				for (int k = 0; k < dates.size();k++) {
					String actualDate = dates.get(i).getText();
					if (actualDate.equalsIgnoreCase(expectedDate))
						dates.get(i).click();
					break;

				}
			} else {
				driver.findElement(By.xpath("//a[@class='ui-datepicker-next ui-corner-all']")).click();
			}

			i++;

		}
	}

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "Y:\\chrome 90\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get("http:\\www.spicejet.com");
		driver.manage().window().maximize();
		Thread.sleep(2000);
//***********************one way scenario*************************
// check one way radio button is default selected

		boolean oneWay = driver.findElement(By.xpath("//input[@id='ctl00_mainContent_rbtnl_Trip_0']")).isEnabled();
		System.out.println("is one way enable? : " + oneWay);
		Assert.assertTrue(oneWay);

		// check return date is default disabled

		String disableReturnDate = driver.findElement(By.xpath("//div[@id='Div1']")).getAttribute("style");
		System.out.println(disableReturnDate);
		if (disableReturnDate.contains("0.5")) {
			System.out.println("return date is disabled");
			Assert.assertTrue(true);
		} else {
			Assert.assertTrue(false);
		}

// ******************select departure and arrival city**************************
		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_ddl_originStation1_CTXT']")).click();
		driver.findElement(By.xpath("//a[text()=' Ranchi (IXR)']")).click();
		Thread.sleep(2000);
// using index concepts
		driver.findElement(By.xpath("(//a[text()=' Kolkata (CCU)'])[2]")).click();
		Thread.sleep(2000);

// using parent child concepts

		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_ddl_destinationStation1_CTXT']")).click();
		driver.findElement(By
				.xpath("//div[@id='glsctl00_mainContent_ddl_destinationStation1_CTNR'] //a[text()=' Srinagar (SXR)']"))
				.click();

// NOTE: In parent child concept we need to provide parent xpath and then give a space and lastly provide child xpath

//********select current date from calender**************
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[@class='ui-state-default ui-state-highlight ui-state-active']")).click();

//******************provide passengers details**************************

		driver.findElement(By.xpath("//div[@id='divpaxinfo']")).click();
		Thread.sleep(2000);
		Select adult = new Select(driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_Adult']")));
		adult.selectByVisibleText("5");

		Select child = new Select(driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_Child']")));
		child.selectByValue("2");

		Select infant = new Select(driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_Infant']")));
		infant.selectByIndex(4);
		Thread.sleep(2000);

//**********validate INR is selected as currency**********
		Select currencies = new Select(
				driver.findElement(By.xpath("//select[@id='ctl00_mainContent_DropDownListCurrency']")));
		String currency = currencies.getFirstSelectedOption().getText();

		System.out.println("current is selected as " + currency);
		Assert.assertEquals(currency, "INR");

//*************search flight*******************************
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_btn_FindFlights']")).click();
		Thread.sleep(2000);

//****************************** Auto-suggestive dropdown*******************

		driver.get("https:\\rahulshettyacademy.com/dropdownsPractise/");
		driver.manage().window().maximize();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@id='autosuggest']")).sendKeys("in");
		Thread.sleep(2000);
		List<WebElement> allOptions = driver.findElements(By.xpath("//li[@class='ui-menu-item'] //a"));
		for (WebElement option : allOptions) {
			String options = option.getText();
			System.out.println(options);
			if (options.equalsIgnoreCase("Guinea-Bissau")) {
				option.click();
			}
		}

//***************Round trip scenario********************

		driver.get("http:\\www.spicejet.com");
		driver.manage().window().maximize();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_rbtnl_Trip_1']")).click();
		Thread.sleep(2000);

		String enaableReturnDate = driver.findElement(By.xpath("//div[@id='Div1']")).getAttribute("style");
		System.out.println(enaableReturnDate);
		if (enaableReturnDate.contains("1")) {
			System.out.println("return date is enaabled");
			Assert.assertTrue(true);
		} else {
			Assert.assertTrue(false);

		}

// ******************select departure and arrival city**************************
		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_ddl_originStation1_CTXT']")).click();
		driver.findElement(By.xpath("//a[text()=' Pune (PNQ)']")).click();
		Thread.sleep(2000);

		driver.findElement(
				By.xpath("//div[@id='glsctl00_mainContent_ddl_destinationStation1_CTNR'] //a[text()=' Goa (GOI)']"))
				.click();

// ********select depature and return date from calender**************
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[@class='ui-state-default ui-state-highlight ui-state-active ui-state-hover']"))
				.click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_view_date2']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[@class='ui-state-default ui-state-active']")).click();

// ******************provide passengers details**************************

		driver.findElement(By.xpath("//div[@id='divpaxinfo']")).click();
		Thread.sleep(2000);
		Select adult1 = new Select(driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_Adult']")));
		adult1.selectByVisibleText("6");

		Select child1 = new Select(driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_Child']")));
		child1.selectByValue("3");

		Select infant1 = new Select(driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_Infant']")));
		infant1.selectByIndex(4);
		Thread.sleep(2000);

// **********validate INR is selected as currency**********
		Select currencies1 = new Select(
				driver.findElement(By.xpath("//select[@id='ctl00_mainContent_DropDownListCurrency']")));
		String currency1 = currencies1.getFirstSelectedOption().getText();
		
		System.out.println("current is selected as " + currency1);
		Assert.assertEquals(currency1, "INR");

// *************search flight*******************************
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_btn_FindFlights']")).click();
		Thread.sleep(2000);

	}

}
