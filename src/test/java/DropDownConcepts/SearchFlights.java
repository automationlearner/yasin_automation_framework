package DropDownConcepts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchFlights {

	WebDriver driver;
	WebDriverWait wait;

	public void selectjourneyDate(WebDriver driver, WebDriverWait wait, String dateOfJourney, String monthOfJourney) {

		int i = 0;
		while (i < 8) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//div[@class='ui-datepicker-group ui-datepicker-group-first']//span[@class='ui-datepicker-month'] | //div[@class='ui-datepicker-group ui-datepicker-group-last']//span[@class='ui-datepicker-month']")));

			List<WebElement> months = driver.findElements(By.xpath(
					"//div[@class='ui-datepicker-group ui-datepicker-group-first']//span[@class='ui-datepicker-month'] | //div[@class='ui-datepicker-group ui-datepicker-group-last']//span[@class='ui-datepicker-month']"));

			String MONTH_BLOCK_FIRST = months.get(0).getText();
			String MONTH_BLOCK_LAST = months.get(1).getText();
			System.out.println(MONTH_BLOCK_FIRST);
			System.out.println(MONTH_BLOCK_LAST);

			if (MONTH_BLOCK_FIRST.equalsIgnoreCase(monthOfJourney)) {
				System.out.println("found in 1st block");

				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@class='ui-datepicker-group ui-datepicker-group-first']//table//tbody/tr/td/a")));

				List<WebElement> dates = driver.findElements(By
						.xpath("//div[@class='ui-datepicker-group ui-datepicker-group-first']//table//tbody/tr/td/a"));

				for (WebElement date : dates) {
					String ALL_DATES = date.getText();
					System.out.println(ALL_DATES);
					if (ALL_DATES.equalsIgnoreCase(dateOfJourney)) {
						date.click();

						break;
					}

				}

			}

			if (MONTH_BLOCK_LAST.equalsIgnoreCase(monthOfJourney)) {
				System.out.println("found in 2nd block");

				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@class='ui-datepicker-group ui-datepicker-group-last']//table//tbody/tr/td/a")));

				List<WebElement> dates = driver.findElements(
						By.xpath("//div[@class='ui-datepicker-group ui-datepicker-group-last']//table//tbody/tr/td/a"));

				for (WebElement date : dates) {
					String ALL_DATES = date.getText();
					System.out.println(ALL_DATES);
					if (ALL_DATES.equalsIgnoreCase(dateOfJourney)) {
						date.click();

						break;
					}

				}

			} else if (driver.findElement(By.xpath("//span[@class='ui-icon ui-icon-circle-triangle-e']"))
					.isDisplayed()) {
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath("//span[@class='ui-icon ui-icon-circle-triangle-e']")));

				driver.findElement(By.xpath("//span[@class='ui-icon ui-icon-circle-triangle-e']")).click();
			}

			i++;

		}

	}

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "Y:\\chrome 90\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		SearchFlights object = new SearchFlights();

		driver.get("https://www.spicejet.com/");
		driver.manage().window().maximize();

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//input[@id='ctl00_mainContent_ddl_originStation1_CTXT']")));
		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_ddl_originStation1_CTXT']")).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()=' Ajmer (KQH)']")));
		driver.findElement(By.xpath("//a[text()=' Ajmer (KQH)']")).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[text()=' Durgapur (RDP)'])[2]")));
		driver.findElement(By.xpath("(//a[text()=' Durgapur (RDP)'])[2]")).click();

		object.selectjourneyDate(driver, wait, "24", "February"); // provide journey date and month

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//input[@name='ctl00$mainContent$btn_FindFlights']")));
		driver.findElement(By.xpath("//input[@name='ctl00$mainContent$btn_FindFlights']")).click();
	}

}
