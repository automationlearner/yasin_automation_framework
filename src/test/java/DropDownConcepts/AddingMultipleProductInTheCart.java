package DropDownConcepts;

import java.util.Arrays;
import java.util.List;

import javax.print.attribute.IntegerSyntax;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddingMultipleProductInTheCart {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "Y:\\chrome 90\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get("https:\\rahulshettyacademy.com/seleniumPractise/");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
		String [] vegiesToPurchase = {"Cauliflower","Corn","Beans","Beetroot"};
		int totalListOfItems=4;
		int searchIndex=0;
		List<WebElement> allItems = driver.findElements(By.xpath("//h4[@class='product-name']"));
		for(int i=0;i<allItems.size();i++) {
			String [] itemName = allItems.get(i).getText().split("-");
			String formattedItemName = itemName[0].trim();
			List<String> purcahseItem =Arrays.asList(vegiesToPurchase);
			
			if(purcahseItem.contains(formattedItemName)) {
				searchIndex++;
				driver.findElement(By.xpath("//button[text()='ADD TO CART']")).click();
				if(totalListOfItems==searchIndex) {
					break;
				}
				
			}
		}
		
String totalItemAdded = driver.findElement(By.xpath("//table/tbody/tr[1]/td[3]")).getText();
int itemsAdded= Integer.parseInt(totalItemAdded);
if (itemsAdded==totalListOfItems) {
	System.out.println("Total no of item added in the cart = "+itemsAdded);
}
	}

}
