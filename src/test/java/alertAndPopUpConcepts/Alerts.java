package alertAndPopUpConcepts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import utilities.ConfigurationClass;

public class Alerts extends ConfigurationClass{
	

	WebDriver driver = invokeSeleniumSetUp();
	
	@Test
	public void alertConcepts() {
		driver.get("http://demo.guru99.com/test/delete_customer.php");			
		
	      	
        driver.findElement(By.name("cusid")).sendKeys("53920");					
        driver.findElement(By.name("submit")).submit();			
        System.out.println( driver.switchTo().alert().getText());
        driver.switchTo().alert().accept();
        
        
        		
	}
	
	@Test
	public void handleAuthenticationAlertPopUp() {
		// url format= https://username:password@url
		
		String userName= "admin";
		String password = "admin";
		
		driver.get("htpps://"+userName+":"+password+"@"+"the-internet.herokuapp.com/basic_auth");
		System.out.println(driver.getTitle());
			
	}

}
