package ActionClassesInSelenium;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import utilities.ConfigurationClass;

public class IFrameConcepts extends ConfigurationClass {
	WebDriver driver = invokeSeleniumSetUp();
	@Test
	public void dragAndDropIframeConcepts() throws InterruptedException {
		driver.get("http:\\jqueryui.com/droppable/");
		driver.manage().window().maximize();
		Thread.sleep(3000);

		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@class='demo-frame']")));
		Thread.sleep(2000);
		Actions go = new Actions(driver);
		WebElement source = driver.findElement(By.xpath("//div[@id='draggable']"));
		WebElement target = driver.findElement(By.xpath("//div[@id='droppable']"));
		go.dragAndDrop(source, target).build().perform();
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("//a[text()='Autocomplete']")).click();
		
	}

	@Test
	public void switchIframeThroughIdAndName() throws InterruptedException {
		
		driver.get("http://demo.guru99.com/test/guru99home/");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
		driver.switchTo().frame("a077aa5e");
		
		driver.findElement(By.xpath("/html/body/a/img")).click();
		
		driver.switchTo().defaultContent();
		
		
		
	}
	
	@Test
	public void switchIframeThroughindex() throws InterruptedException {
		
		driver.get("http://demo.guru99.com/test/guru99home/");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
		List<WebElement> iframes = driver.findElements(By.xpath("//iframe"));
		int element_index=0;
		for (int i= 0;i<iframes.size();i++) {
			int location = driver.findElements(By.xpath("/html/body/a/img")).size();
			
			if (location==1) {
				break;
			}
			
			element_index++;
		}
		System.out.println("index of iframe is : "+element_index);
		int iframe_Index = element_index-1;
		
		driver.switchTo().frame(iframe_Index);
		
		driver.findElement(By.xpath("/html/body/a/img")).click();
		System.out.println(driver.getTitle());
		driver.switchTo().defaultContent();
		Select courseSelection = new Select(driver.findElement(By.xpath("//select")));
		courseSelection.selectByIndex(2);
		
		Set<String> windows = driver.getWindowHandles();
		Iterator<String> go = windows.iterator();
		String parentWindow = go.next();
		String childWindow = go.next();
		driver.switchTo().window(childWindow);
		System.out.println(driver.findElement(By.xpath("//*[@id=\"g-mainbar\"]/div/div/div/div/div/div/div[2]/text()[1]")).getText());
		
		driver.switchTo().window(parentWindow);
		courseSelection.selectByIndex(3);
		
		
		
		
		
	}

}
