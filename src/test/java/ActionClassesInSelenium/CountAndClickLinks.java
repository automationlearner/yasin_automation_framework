package ActionClassesInSelenium;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CountAndClickLinks {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "Y:\\chrome 90\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get("https://www.amazon.com/");
		driver.manage().window().maximize();
		Thread.sleep(3000);

		List<WebElement> links = driver.findElements(By.xpath("//a"));
		int size = links.size();
		System.out.println(size);
		// WebElement footerDriver =
		// driver.findElement(By.xpath("//div[@class='navFooterVerticalRow
		// navAccessibility']"));
		List<WebElement> footerlinks = driver
				.findElements(By.xpath("//div[@class='navFooterVerticalRow navAccessibility']//a"));
		System.out.println(footerlinks.size());
		// WebElement FirstFooterDriver =
		// footerDriver.findElement(By.xpath("//div[@class='navFooterLinkCol
		// navAccessibility'][1]"));
		List<WebElement> firstfooterLinks = driver
				.findElements(By.xpath("//div[@class='navFooterLinkCol navAccessibility'][1]//a"));
		System.out.println(firstfooterLinks.size());
		for (int i = 0; i < firstfooterLinks.size(); i++) {
			String openIntabs = Keys.chord(Keys.CONTROL, Keys.ENTER);
			firstfooterLinks.get(i).sendKeys(openIntabs);

		}
		String MAIN_WINDOW = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		Iterator<String> it = windows.iterator();

		int totalwindows = windows.size();
		System.out.println(totalwindows);
		
		while (it.hasNext()) {

			driver.switchTo().window(it.next());
			
			
			System.out.println(driver.getTitle());
		}
		driver.switchTo().window(MAIN_WINDOW);
		Set<String> windows1 = driver.getWindowHandles();
		Iterator<String> it1 = windows1.iterator();
		while(it1.hasNext()) {
			driver.switchTo().window(it1.next());
	        	if(driver.getTitle().equalsIgnoreCase("Amazon.com, Inc. - Overview")) {
	        		
	        		System.out.println("LANDED INTO WORKSPACE PAGE "+driver.getTitle());
	        		
	        	}
		}
		
        
	
}

	}


