package stepDefinations;

import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import spiceJetObjects.SpicejetHomepage;
import utilities.ConfigurationClass;

@RunWith(Cucumber.class)
public class FlightsearchingUsingCucumber extends ConfigurationClass {

	WebDriver driver = invokeSeleniumSetUp();
	SpicejetHomepage homepageObj = new SpicejetHomepage();
	WebDriverWait wait = new WebDriverWait(driver, 20);
	@Given("^land into spicejet hoempage with url \"([^\"]*)\"$")
	public void land_into_spicejet_hoempage_with_url_something(String SPICEJET_URL) throws Throwable {

		homepageObj.navigateToWebsite(driver, SPICEJET_URL);
	}

	@When("^user enter departure city$")
	public void user_enter_departure_city() throws Throwable {
		homepageObj.destinationSelectBox(driver).click();
		homepageObj.selectDepartureCity(driver).click();
	}

	@And("^user enter destination city$")
	public void user_enter_destination_city() throws Throwable {
		homepageObj.selectDepartureCity(driver).click();
	}

	@And("^select date of journey$")
	public void select_date_of_journey() throws Throwable {
		homepageObj.selectDateFromCalender(driver, "December", "15");
		
	}

	@And("^search flight by clicking search button$")
	public void search_flight_by_clicking_search_button() throws Throwable {

		homepageObj.searchFlights(driver).click();
	}

	@Then("^user should be able to search flights$")
	public void user_should_be_able_to_search_flights() throws Throwable {

		System.out.println("searched filght successfully");

	}
}
