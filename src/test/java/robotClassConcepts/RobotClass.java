package robotClassConcepts;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.testng.annotations.Test;

import utilities.ConfigurationClass;

public class RobotClass extends ConfigurationClass {
	
	WebDriver driver = invokeSeleniumSetUp();

	@Test
	public void robotHandles() throws AWTException {
		driver.get("http://demo.guru99.com/test/tooltip.html");
		driver.manage().window().maximize();
		
		
		Robot robot = new Robot();
		
		robot.mouseMove(30,100);
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		
		
		
		
		
	}

}
