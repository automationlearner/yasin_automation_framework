package WindowHandles;

import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class SwitchMultipleWindows {
	
	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "Y:\\chrome 90\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https:\\rahulshettyacademy.com/loginpagePractise/");
		driver.manage().window().maximize();
		Thread.sleep(3000);

		driver.findElement(By.xpath("//a[text()='Free Access to InterviewQues/ResumeAssistance/Material']")).click();
		
		String mainWindow = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		Iterator<String> go = windows.iterator();
		
		while(go.hasNext()) {
			String childWindow = go.next();
			if(!mainWindow.equalsIgnoreCase(childWindow)) {
				driver.switchTo().window(childWindow);
				String emailId = driver.findElement(By.xpath("//a[text()='mentor@rahulshettyacademy.com']")).getText();
				driver.switchTo().window(mainWindow);
				driver.findElement(By.xpath("//input[@id='username']")).sendKeys(emailId);
			
			
			}
		}
		
		
		
		
		
	}

}
