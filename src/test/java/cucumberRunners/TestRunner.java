package cucumberRunners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
		



@RunWith(Cucumber.class)

@CucumberOptions
(features="Y:\\eclipse-workspace\\AutomationSeleniumTesting\\src\\test\\java\\featureFiles",
glue={"stepDefinations"})						
public class TestRunner extends AbstractTestNGCucumberTests	
{		

}