package SeleniumBasics;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import utilities.ConfigurationClass;

public class CssSelectorTechnique extends ConfigurationClass {

	WebDriver driver = invokeSeleniumSetUp();
	

	public void cssSelectorSyntax() {

		// generic syntax
		driver.findElement(By.cssSelector("tag[attribute='value']"));

		// using id
		driver.findElement(By.cssSelector(""));
		// using class
		driver.findElement(By.cssSelector("input[class='mr-sm-2 form-control']"));
		driver.findElement(By.cssSelector("input.mr-sm-2 form-control"));

		// other attributes

		driver.findElement(By.cssSelector("input[placeholder='First Name']"));

		// combine id and other attributes
		driver.findElement(By.cssSelector("input#firstName[placeholder='First Name']"));

		// combine class and other attributes

		driver.findElement(By.cssSelector("textarea.form-control[placeholder='Current Address']"));
		
		//parent-child method
		driver.findElement(By.cssSelector("div>button[id='submit']"));
		
		//when element exists in hierarchy
		driver.findElement(By.cssSelector("select#oldSelectMenu>option:nth-of-type(2)"));
		
		//regular expression
		driver.findElement(By.cssSelector("input[placeholder^=Firs]")); //"^" operator -from 1st to last
		driver.findElement(By.cssSelector("input[placeholder$=ame]"));  //"$" operator --last to 1st
		driver.findElement(By.cssSelector("input[placeholder*='st N']")); //"*" operator - from anywhere
		
		//inner text
		
		
		driver.findElement(By.cssSelector("span:contains(\"Flights\")"));
	
		
		
		
	}

	
	@Test
	public void handonsCssSelector() {
		
		driver.get("https://www.amazon.com/");
		Select drpdown = new Select(driver.findElement(By.cssSelector("select[id='searchDropdownBox']")));
		List<WebElement> options = drpdown.getOptions();
		for(WebElement option: options) {
			System.out.println(option.getText());
		}
		
		
	}
	
}
