package SeleniumBasics;

import org.testng.annotations.Test;

import actionTemplates.ActionDefination;


import org.testng.annotations.BeforeClass;

import java.util.*;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class BasicsSeleniumConcepts extends ActionDefination {
	WebDriver driver;
	WebElement element1;

	

	@BeforeClass
	public void seleniumSetupConfiguration() {
		System.setProperty("webdriver.chrome.driver", "Y:\\chrome 90\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test(groups = "basic")
	public void fewBasicSeleniumConcepts() throws InterruptedException {
		String baseUrl = "https://www.google.com/";
		String expectedTitle = "Google";
		navigateToWebsite(driver, baseUrl);

		String title = driver.getTitle();
		System.out.println("Title of the page is : " + title);
		if (expectedTitle.equals(title)) {
			System.out.println("Test Passed");
		} else {
			System.out.println("Test failed");
		}

		// fInd element concept in selenium

		// 1. using name locator
		element1 = driver.findElement(By.name("q"));
		element1.sendKeys("flipkart");
		Thread.sleep(2000);

		// 2. using class name locator
		driver.findElement(By.className("gNO89b")).click();
		Thread.sleep(2000);

		// fInd elements concept in selenium
		List<WebElement> allLinks = driver.findElements(By.tagName("a"));
		System.out.println("Total no of links in the website are : " + allLinks.size());
		for (WebElement links : allLinks) {

			System.out.println("********************************names of links are ***************");
			System.out.println(links.getText());
			System.out.println("****************************Attribute of the links are*******************");
			System.out.println(links.getAttribute("href"));

		}
		Thread.sleep(5000);
		
		
		
		
		
		
		driver.findElement(By.cssSelector("img[src='/images/branding/googlelogo/2x/googlelogo_color_92x30dp.png']"))
				.click();

		
		
		
	}



	

}
