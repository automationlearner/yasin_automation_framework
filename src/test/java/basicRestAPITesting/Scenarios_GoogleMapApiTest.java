/*
 * package basicRestAPITesting;
 * 
 * import io.restassured.RestAssured;
 * 
 * import io.restassured.http.ContentType; import io.restassured.parsing.Parser;
 * 
 * import io.restassured.response.Response; import
 * pojoClasses.GoogleAddPlaceRequestBody_POJO; import
 * pojoClasses.GoogleAddPlaceResponseBody_POJO; import
 * pojoClasses.GoogleUpdatePlaceRequestBody_POJO; import
 * pojoClasses.GoogleUpdatePlaceResponseBody_POJO; import
 * pojoClasses.Locations_POJO;
 * 
 * import static io.restassured.RestAssured.*;
 * 
 * import java.util.ArrayList; import java.util.List;
 * 
 * 
 * 
 * public class Scenarios_GoogleMapApiTest {
 * 
 * // Serialization GoogleAddPlaceRequestBody_POJO requestBody = new
 * GoogleAddPlaceRequestBody_POJO(); GoogleAddPlaceResponseBody_POJO
 * responseBody = new GoogleAddPlaceResponseBody_POJO(); Locations_POJO location
 * = new Locations_POJO();
 * 
 * GoogleUpdatePlaceRequestBody_POJO updatePlaceReqBody = new
 * GoogleUpdatePlaceRequestBody_POJO(); GoogleUpdatePlaceResponseBody_POJO
 * updatePlaceResBody = new GoogleUpdatePlaceResponseBody_POJO(); public
 * Locations_POJO jsonLoactionBody() { location.setLat(-38.383494);
 * location.setLng(33.427362); return location; }
 * 
 * public List<String> jsonTypesBody() { List<String> typeBody = new
 * ArrayList<String>();
 * 
 * typeBody.add("shoe park"); typeBody.add("shop"); return typeBody; }
 * 
 * public GoogleAddPlaceRequestBody_POJO jsonRequestBody() {
 * requestBody.setName("Frontline house");
 * requestBody.setLocation(jsonLoactionBody()); requestBody.setAccuracy(50);
 * requestBody.setPhone_number("(+91) 983 893 3937");
 * requestBody.setAddress("29, side layout, cohen 09");
 * requestBody.setTypes(jsonTypesBody());
 * requestBody.setWebsite("http://google.com");
 * requestBody.setLanguage("French-IN"); return requestBody; }
 * 
 * public GoogleUpdatePlaceRequestBody_POJO jsonRequestBody_updatePlace(String
 * place_id) {
 * 
 * updatePlaceReqBody.setAddress("70 winter walk, USA");
 * updatePlaceReqBody.setKey("qaclick123");
 * updatePlaceReqBody.setPlace_id(place_id); return updatePlaceReqBody;
 * 
 * 
 * }
 * 
 * //@Test public void apiOperation_ADD_UPDATE_GET() {
 * 
 * 
 * String payload = "{\r\n" + "  \"location\": {\r\n" +
 * "    \"lat\": -38.383494,\r\n" + "    \"lng\": 33.427362\r\n" + "  },\r\n" +
 * "  \"accuracy\": 50,\r\n" + "  \"name\": \"Frontline house\",\r\n" +
 * "  \"phone_number\": \"(+91) 983 893 3937\",\r\n" +
 * "  \"address\": \"29, side layout, cohen 09\",\r\n" + "  \"types\": [\r\n" +
 * "    \"shoe park\",\r\n" + "    \"shop\"\r\n" + "  ],\r\n" +
 * "  \"website\": \"http://google.com\",\r\n" +
 * "  \"language\": \"French-IN\"\r\n" + "}\r\n" + "";
 * 
 * 
 * RestAssured.baseURI = "https://rahulshettyacademy.com"; responseBody =
 * given().queryParam("key", "qaclick123").body(jsonRequestBody()).expect()
 * .defaultParser(Parser.JSON).when().post("/maps/api/place/add/json")
 * .as(GoogleAddPlaceResponseBody_POJO.class);
 * 
 * String placeid = responseBody.getPlace_id();
 * 
 * System.out.println("place id of the response is :" + placeid);
 * 
 * // update place
 * 
 * 
 * String updatePayload = "{\r\n" + "\"place_id\":" + placeid + ",\r\n" +
 * "\"address\":\"70 winter walk, USA\",\r\n" + "\"key\":\"qaclick123\"\r\n" +
 * "}";
 * 
 * 
 * 
 * 
 * updatePlaceResBody = given().queryParam("Key",
 * "qaclick123").queryParam("place_id", placeid)
 * .body(jsonRequestBody_updatePlace(placeid)).expect().defaultParser(Parser.
 * JSON).when().put("/maps/api/place/update/json").as(
 * GoogleUpdatePlaceResponseBody_POJO.class);
 * 
 * String expectedResponseMessage = updatePlaceResBody.getMsg();
 * System.out.println("Message of response :"+expectedResponseMessage);
 * 
 * //Assert.assertEquals("Address successfully updated",expectedResponseMessage)
 * ; Response getResponse = given().log().all().queryParam("Key",
 * "qaclick123").queryParam("place_id", placeid)
 * .when().get("/maps/api/place/get/json").then().log().all().assertThat().
 * contentType(ContentType.JSON) .extract().response();
 * 
 * String getPlaceResBody = getResponse.asString();
 * System.out.println(getPlaceResBody);
 * 
 * }
 * 
 * }
 */