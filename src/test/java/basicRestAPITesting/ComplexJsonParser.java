package basicRestAPITesting;



import io.restassured.path.json.JsonPath;

public class ComplexJsonParser {
	
	//@Test
	public void jsonParser() {
		
		
		JsonPath js = new JsonPath(ComplexJsonBody.complexPayLoad);
		
		String TOPPING_TYPE = js.getString("topping[4].type");
		System.out.println(TOPPING_TYPE);
		int TOPPING_ID = js.getInt("topping[4].id");
		System.out.println(TOPPING_ID);
		String BATTERS_TYPE = js.getString("batters.batter[2].type");
		System.out.println(BATTERS_TYPE);
		double ppu = js.getDouble("ppu");
		System.out.println(ppu);
	}

}
